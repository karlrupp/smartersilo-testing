
import smbus
import time
import numpy
import matplotlib.pyplot as pyplot

# Get I2C bus
bus = smbus.SMBus(1)

#
# Part 1: Read values from tester PCB:
#

addr_tester = 0x41

# Read voltages:
analog_3p3V  = bus.read_byte_data(addr_tester, 0x00) / 128.0 * 3.3;
analog_1p5V  = bus.read_byte_data(addr_tester, 0x01) / 128.0 * 3.3;
digital_3p3V = bus.read_byte_data(addr_tester, 0x02) / 128.0 * 3.3;

print("Analog voltage 3.3V: " + str(analog_3p3V) + " Volt")
print("Analog voltage 1.5V: " + str(analog_1p5V) + " Volt")
print("Digital voltage 3.3V: " + str(digital_3p3V) + " Volt")

ctrl2 = bus.read_byte_data(addr_tester, 0x03)
ctrl1 = bus.read_byte_data(addr_tester, 0x04)
ctrl0 = bus.read_byte_data(addr_tester, 0x05)

print("Ctrl: " + str(ctrl2) + " " + str(ctrl1) + " " + str(ctrl0) )

gain0 = bus.read_byte_data(addr_tester, 0x06)
gain1 = bus.read_byte_data(addr_tester, 0x07)
gain2 = bus.read_byte_data(addr_tester, 0x08)

print("Gain: " + str(gain2) + " " + str(gain1) + " " + str(gain0) )

dummy = bus.read_byte_data(addr_tester, 0x09)
print(dummy)

#################

#
# Part 2: Run ultrasonic measurements
#

envelope_len = 170
xvalues = numpy.linspace(0, 6, envelope_len)

addr = 0x42

#default values:
bus.write_byte_data(addr, 0x04,  8)  # divisor
bus.write_byte_data(addr, 0x03,  8)  # num measurements
bus.write_byte_data(addr, 0x02,  8)  # num packets
bus.write_byte_data(addr, 0x01,  2)  # gain


while 1:

  # Measurement 1: Use gain 2:
  bus.write_byte_data(addr, 0x01,    2)
  bus.write_byte_data(addr, 0x00, 0x01)

  time.sleep(1.2)

  envelope_2 = []
  for i in range(0, envelope_len):
    envelope_2.append(bus.read_byte_data(addr, 0x10 + i))
  pyplot.plot(xvalues, envelope_2, label="Gain 2", color='b')

  # Measurement 2: Use gain 1:
  bus.write_byte_data(addr, 0x01,    1)
  bus.write_byte_data(addr, 0x00, 0x01)

  time.sleep(1.2)

  envelope_1 = []
  for i in range(0, envelope_len):
    envelope_1.append(bus.read_byte_data(addr, 0x10 + i))
  pyplot.plot(xvalues, envelope_1, label="Gain 1", color='r')

  # plot results
  pyplot.title("Ultrasonic Received Envelope")
  pyplot.xlabel("Meter (uncalibrated)")
  pyplot.ylabel("ADC Value")
  pyplot.show()

