
from matplotlib.ticker import NullFormatter  # useful for `logit` scale
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import PySimpleGUI as sg
import os
import matplotlib
matplotlib.use('TkAgg')

import smbus
import time


#
# Accelerometer readouts
#

def accelerometer_read():
  bus = smbus.SMBus(1)
  addr = 0x18

  xAccl = 16000
  yAccl = 16000
  zAccl = 16000

  try:
    # Set accelerometer active:
    bus.write_byte_data(addr, 0x20, 0x27)
    bus.write_byte_data(addr, 0x23, 0x08)

    time.sleep(0.1)

    # Read 7 bytes from device, starting at register 0x00:
    status = bus.read_byte_data(addr, 0x27)

    # Convert the data
    xdata0 = bus.read_byte_data(addr, 0x28)
    xdata1 = bus.read_byte_data(addr, 0x29)
    xAccl = xdata1 * 256 + xdata0
    if xAccl > 32767 :
      xAccl -= 65536

    ydata0 = bus.read_byte_data(addr, 0x2A)
    ydata1 = bus.read_byte_data(addr, 0x2B)
    yAccl = ydata1 * 256 + ydata0
    if yAccl > 32767 :
      yAccl -= 65536

    zdata0 = bus.read_byte_data(addr, 0x2C)
    zdata1 = bus.read_byte_data(addr, 0x2D)
    zAccl = zdata1 * 256 + zdata0
    if zAccl > 32767 :
      zAccl -= 65536

    # Output data
    #print("Acceleration in X-Axis : %d" %xAccl)
    #print("Acceleration in Y-Axis : %d" %yAccl)
    #print("Acceleration in Z-Axis : %d" %zAccl)
  except:
    print("I/O error when reading accelerometer!")

  return xAccl, yAccl, zAccl


#
# Voltage readouts
#

def voltages_read():

  bus = smbus.SMBus(1)
  addr_tester = 0x41

  analog_3p3V = 0
  analog_1p5V = 0
  digital_3p3V = 0

  try:
    analog_3p3V  = bus.read_byte_data(addr_tester, 0x00) / 128.0 * 3.3;
    analog_1p5V  = bus.read_byte_data(addr_tester, 0x01) / 128.0 * 3.3;
    digital_3p3V = bus.read_byte_data(addr_tester, 0x02) / 128.0 * 3.3;

  except:
    print("I/O error when reading accelerometer!")

  return analog_3p3V, analog_1p5V, digital_3p3V


# ------------------------------- PASTE YOUR MATPLOTLIB CODE HERE -------------------------------
figure_x = 0
figure_y = 0
figure_w = 0
figure_h = 0

def ultrasonicplot(init):

  envelope_len = 180
  xvalues = np.linspace(0, 5, envelope_len)

  bus = smbus.SMBus(1)
  addr = 0x42

  envelope_2 = []
  envelope_1 = []

  if init == 0:
    try:
      #default values:
      bus.write_byte_data(addr, 0x07,  0)  # small-distance
      bus.write_byte_data(addr, 0x05,  8)  # overscan
      bus.write_byte_data(addr, 0x04,  32)  # divisor
      bus.write_byte_data(addr, 0x03,  8)  # num measurements
      bus.write_byte_data(addr, 0x02,  4)  # num packets
      bus.write_byte_data(addr, 0x01,  2)  # gain

      # Measurement 1: Use gain 2:
      bus.write_byte_data(addr, 0x01,    8)
      bus.write_byte_data(addr, 0x00, 0x01)

      time.sleep(1.0)

      for i in range(0, envelope_len):
        envelope_2.append(bus.read_byte_data(addr, 0x10 + i))

      # Measurement 2: Use gain 1:
      bus.write_byte_data(addr, 0x01,    2)
      bus.write_byte_data(addr, 0x00, 0x01)

      time.sleep(1.0)

      for i in range(0, envelope_len):
        envelope_1.append(bus.read_byte_data(addr, 0x10 + i))

    except:
      for i in range(0, envelope_len):
        envelope_1.append(0)
        envelope_2.append(0)
  else:
    for i in range(0, envelope_len):
      envelope_1.append(0)
      envelope_2.append(0)

  # plot with various axes scales
  plt.figure(1, figsize=(9,4.5))
  plt.clf()

  plt.plot(xvalues, envelope_1, label="Gain 2")
  plt.plot(xvalues, envelope_2, label="Gain 8")
  plt.ylabel('Received Signal Strength')
  plt.xlabel('Meter (uncalibrated)')
  plt.yscale('linear')
  plt.title('Ultrasonic Signal')
  plt.legend()

  fig = plt.gcf()      # if using Pyplot then get the figure from the plot
  figure_x, figure_y, figure_w, figure_h = fig.bbox.bounds
  return fig

# ------------------------------- Beginning of Matplotlib helper code -----------------------


def draw_figure(canvas, figure, loc=(0, 0)):
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    return figure_canvas_agg
# ------------------------------- Beginning of GUI CODE -------------------------------


def run_test(window):
    #
    # Reset text fields
    #
    window['Result'].Update("Running tests...", background_color="yellow"); 
    window.Read(timeout=0)  #non-blocking update of GUI elements

    #
    # Read current values and set text fields accordingly
    #

    check_ok = 1

    ## Checking voltages:

    analog_3p3V, analog_1p5V, digital_3p3V = voltages_read()

    window['Vanalog3.3'].Update('{:.2f}'.format(analog_3p3V) + " Volt");
    if analog_3p3V < 3.4 and analog_3p3V > 3.2:
      window['Vanalog3.3Status'].Update("OK", background_color="green")
    else:
      window['Vanalog3.3Status'].Update("FAIL - check U4 and U7.", background_color="red")
      check_ok = 0

    window['Vanalog1.5'].Update('{:.2f}'.format(analog_1p5V) + " Volt");
    if analog_1p5V < 1.6 and analog_1p5V > 1.4:
      window['Vanalog1.5Status'].Update("OK", background_color="green")
    else:
      window['Vanalog1.5Status'].Update("FAIL - check U5 and U7.", background_color="red")
      check_ok = 0

    window['Vdigital3.3'].Update('{:.2f}'.format(digital_3p3V) + " Volt");
    if digital_3p3V < 3.4 and digital_3p3V > 3.2:
      window['Vdigital3.3Status'].Update("OK", background_color="green")
    else:
      window['Vdigital3.3Status'].Update("FAIL - check U3 and U7.", background_color="red")
      check_ok = 0


    ## Checking accelerometer:

    acc_x, acc_y, acc_z = accelerometer_read();
    window['Acceleration'].Update(str(acc_x) + "," + str(acc_y) + "," + str(acc_z));
    if acc_x < 3000 and acc_x > -3000 and acc_y < 3000 and acc_y > -3000 and acc_z < 13000:
      window['AccelerationStatus'].Update("OK", background_color="green")
    else:
      window['AccelerationStatus'].Update("FAIL - check U6.", background_color="red")
      check_ok = 0
    

    ## Adding the plot to the window:

    fig = ultrasonicplot(0)
    fig_canvas_agg.draw()

    if check_ok == 1:
      window['Result'].Update("Testing successful!", background_color="green"); 
    else:
      window['Result'].Update("Testing failed!", background_color="red"); 

    ## Consistency check:
    bus = smbus.SMBus(1)
    addr_tester = 0x41
    try:
      ctrl2 = bus.read_byte_data(addr_tester, 0x03)
      ctrl1 = bus.read_byte_data(addr_tester, 0x04)
      ctrl0 = bus.read_byte_data(addr_tester, 0x05)

      if ctrl2 != 0 or ctrl1 != 1 or ctrl0 != 0:
        print("Ctrl: " + str(ctrl2) + " " + str(ctrl1) + " " + str(ctrl0) )
        throw

    except:
      window['Result'].Update("Ctrl consistency check failed! Check M1.", background_color="red");

    try:
      gain0 = bus.read_byte_data(addr_tester, 0x06)
      gain1 = bus.read_byte_data(addr_tester, 0x07)
      gain2 = bus.read_byte_data(addr_tester, 0x08)

      if gain2 != 0 or gain1 != 0 or gain0 != 1:
        print("Gain: " + str(gain2) + " " + str(gain1) + " " + str(gain0) )
        throw

    except:
      window['Result'].Update("Gain consistency check failed! Check U2.", background_color="red");



#
# Programming the PCB:
#
def program_pcb(window):

  window['Result'].Update("Programming...", background_color="yellow"); 
  window.Read(timeout=0)  #non-blocking update of GUI elements
  prog_output = os.popen("python3 pyupdi/pyupdi.py -d tiny1614 -c /dev/ttyAMA0 -f ultrasonic.hex").read()
  if prog_output.startswith("Programming successful"):
    window['Result'].Update("Programming successful!", background_color="green")
  else:
    window['Result'].Update("Programming failed! Check IC1.", background_color="red")



#
# Main execution stream
#

os.chdir(os.path.dirname(os.path.abspath(__file__)))

layout = [[sg.Text('Analog Voltage (3.3V): ', size=(20, 1)), sg.Text('- Volt', size=(20,1), key='Vanalog3.3'), sg.Text('', size=(50,1), key='Vanalog3.3Status')],
          [sg.Text('Analog Voltage (1.5V): ', size=(20, 1)), sg.Text('- Volt', size=(20,1), key='Vanalog1.5'), sg.Text('', size=(50,1), key='Vanalog1.5Status')],
          [sg.Text('Digital Voltage (3.3V):', size=(20, 1)), sg.Text('- Volt', size=(20,1), key='Vdigital3.3'), sg.Text('', size=(50,1), key='Vdigital3.3Status')],
          [sg.Text('Acceleration:',           size=(20, 1)), sg.Text('-,-,-',  size=(20,1), key='Acceleration'), sg.Text('', size=(50,1), key='AccelerationStatus')],
          [sg.Canvas(size=(figure_w, figure_h), key='canvas')],
          [sg.Button('Program'), sg.Button('Run Test'), sg.Text('', size=(80,1), key='Result', justification="center"), sg.Exit()]]

window = sg.Window('SmarterSilo: Ultrasonic Tester - BrickXter GmbH', layout, finalize=True)      

fig = ultrasonicplot(1)
fig_canvas_agg = draw_figure(window['canvas'].TKCanvas, fig)

while True:
    event, values = window.Read()

    if event == "Run Test":
      run_test(window)

    if event == "Program":
      program_pcb(window)

    if event in (None, 'Exit'):
      break

window.Close()
