
import PySimpleGUI as sg
import os
import subprocess
import time
import smbus


tester_pcb_addr = 0x41
silo_pcb_addr   = 0x42




#
# Programming the PCB:
#
def program_pcb(window):

  window['Result'].Update("Programming...", background_color="yellow"); 
  window.Read(timeout=0)  #non-blocking update of GUI elements


  ##
  ## make fuse
  ##
  args = ['sudo', 'make', 'fuse']

  try:
      # stdout = subprocess.PIPE lets you redirect the output
      res = subprocess.Popen(args, stdout=subprocess.PIPE)
  except OSError:
      window['Result'].Update("Setting fuses failed! Check U2.", background_color="red")
      return 1;

  res.wait() # wait for process to finish; this also sets the returncode variable inside 'res'
  if res.returncode != 0:
      window['Result'].Update("Setting fuses failed! Check U2.", background_color="red")
      return 2
  else:
      window['Result'].Update("Setting fuses successful!", background_color="green")

  window.Read(timeout=0)  #non-blocking update of GUI elements

  ##
  ## make flash
  ##
  args = ['sudo', 'make', 'flash']

  try:
      # stdout = subprocess.PIPE lets you redirect the output
      res = subprocess.Popen(args, stdout=subprocess.PIPE)
  except OSError:
      window['Result'].Update("Flashing microcontroller failed! Check U2.", background_color="red")
      return 3

  res.wait() # wait for process to finish; this also sets the returncode variable inside 'res'
  if res.returncode != 0:
      window['Result'].Update("Flashing microcontroller failed! Check U2.", background_color="red")
      return 4
  else:
      window['Result'].Update("Flashing microcontroller successful!", background_color="green")

  window.Read(timeout=0)  #non-blocking update of GUI elements

  return 0


#
# Programming the PCB:
#
def write_firmware(window, sensorid):

  window['Result'].Update("Flashing firmware...", background_color="yellow");
  window.Read(timeout=0)  #non-blocking update of GUI elements

  ##
  ## Preprocessing step: Generate EEPROM file:
  ##
  sensoridstring = str(sensorid)
  if sensorid < 2100000 or sensorid > 2199999:
      window['Result'].Update("Abort: Invalid sensor ID " + sensoridstring, background_color="red")
      return 3

  f = open("silo.eep","w+")
  checksum = 9;
  f.write(":09000000");
  for elem in sensoridstring:
    f.write("3")
    f.write(elem)
    checksum += int("30", 16) + int(elem)
  f.write("0001")
  checksum = (checksum + 1) % 256
  checksum = (256 - checksum) % 256
  f.write("%0.2X" % checksum)
  f.write("\r\n")
  f.write(":00000001FF")
  f.close()


  ##
  ## flashing firmware
  ##
  args = ['avrdude', '-c', 'usbasp', '-p', 'atmega328pb', '-B5', '-U', 'flash:w:silo.hex:i', '-U', 'eeprom:w:silo.eep:i', '-U', 'lfuse:w:0xFC:m', '-U', 'hfuse:w:0xD9:m']

  try:
      # stdout = subprocess.PIPE lets you redirect the output
      avrdude_output = subprocess.check_output(args,stderr=subprocess.STDOUT)
  except subprocess.CalledProcessError as e:
      sg.PopupError(str(e.output, "utf-8"), title="avrdude error");
      window['Result'].Update("Error in running avrdude!", background_color="red")
      return 3

  window['Result'].Update("Flashing microcontroller successful!", background_color="green")

  window.Read(timeout=0)  #non-blocking update of GUI elements

  return 0

#
# Reads a register byte from the specified I2C device
#
def read_register(i2c_addr, reg_addr):

  value = 0

  try:
    bus = smbus.SMBus(1)
    value = bus.read_byte_data(i2c_addr, reg_addr)

    # Fix for squeezing 10 bits into 8 bits for solar charging shunt resistor
    if i2c_addr == tester_pcb_addr and (reg_addr == 6 or reg_addr == 7):
      value = value + 512

    if value == 0:
      print("value: " + str(value))
      throw
  except:
    print("I/O error when reading register " + str(reg_addr) + " from device " + str(i2c_addr))

  time.sleep(0.1)

  return value


def write_register(i2c_addr, reg_addr, value):
  try:
    bus = smbus.SMBus(1)
    bus.write_byte_data(i2c_addr, reg_addr, value)
  except:
    print("I/O error when writing register " + str(reg_addr) + " from device " + str(i2c_addr))
    return 1

  time.sleep(0.1)

  return 0


#
# Accelerometer readouts
#

def accelerometer_read():
  bus = smbus.SMBus(1)
  addr = 0x19

  xAccl = 16000
  yAccl = 16000
  zAccl = 16000

  try:
    # Set accelerometer active:
    bus.write_byte_data(addr, 0x20, 0x27)
    bus.write_byte_data(addr, 0x23, 0x08)

    time.sleep(0.1)

    # Read 7 bytes from device, starting at register 0x00:
    status = bus.read_byte_data(addr, 0x27)

    # Convert the data
    xdata0 = bus.read_byte_data(addr, 0x28)
    xdata1 = bus.read_byte_data(addr, 0x29)
    xAccl = xdata1 * 256 + xdata0
    if xAccl > 32767 :
      xAccl -= 65536

    ydata0 = bus.read_byte_data(addr, 0x2A)
    ydata1 = bus.read_byte_data(addr, 0x2B)
    yAccl = ydata1 * 256 + ydata0
    if yAccl > 32767 :
      yAccl -= 65536

    zdata0 = bus.read_byte_data(addr, 0x2C)
    zdata1 = bus.read_byte_data(addr, 0x2D)
    zAccl = zdata1 * 256 + zdata0
    if zAccl > 32767 :
      zAccl -= 65536

    # Output data
    #print("Acceleration in X-Axis : %d" %xAccl)
    #print("Acceleration in Y-Axis : %d" %yAccl)
    #print("Acceleration in Z-Axis : %d" %zAccl)
  except:
    print("I/O error when reading accelerometer!")

  return xAccl, yAccl, zAccl




#
# Temperature readouts
#

def temperature_read():
  bus = smbus.SMBus(1)
  addr = 0x49

  temp = -10

  try:
    temp = bus.read_byte_data(addr, 0x00)

    # Convert the data
    if temp > 127:
      temp -= 256

  except:
    print("I/O error when reading temperature!")

  return temp


##########################################

def run_test(window):
    #
    # Reset text fields
    #
    window['VSystem'].Update('-');
    window['Temperature'].Update('-');
    window['Accelerometer'].Update('-');
    window['Solar-idle'].Update('-');
    window['Solar-chrg'].Update('-');
    window['VGSM'].Update('-');
    window['VStepUp'].Update('-');
    window['Result'].Update("Running tests...", background_color="yellow"); 
    window.Read(timeout=0)  #non-blocking update of GUI elements

    #
    # Read current values and set text fields accordingly
    #

    check_ok = 1

    ## Check system voltage:
    voltage = read_register(tester_pcb_addr, 0x02) * 3.3 / 128.0
    voltage = 3.3
    window['VSystem'].Update('{:.2f}'.format(voltage) + " Volt");
    if voltage < 3.0:
      window['VSystemStatus'].Update("FAIL - check power, U1, U4, M4.", background_color="red")
      window['Result'].Update("Testing failed!", background_color="red"); 
      return
    window['VSystemStatus'].Update("SUCCESS", background_color="green")

    window.Read(timeout=0)  #non-blocking update of GUI elements


    ## Checking accelerometer:

    acc_x, acc_y, acc_z = accelerometer_read();
    window['Accelerometer'].Update(str(acc_x) + "," + str(acc_y) + "," + str(acc_z));
    if acc_x < 3000 and acc_x > -3000 and acc_y < 3000 and acc_y > -3000 and acc_z > 13000:
      window['AccelerometerStatus'].Update("OK", background_color="green")
    else:
      window['AccelerometerStatus'].Update("FAIL - check U3.", background_color="red")
      check_ok = 0

    window.Read(timeout=0)  #non-blocking update of GUI elements


    ## Checking temperature:

    temp = temperature_read();
    window['Temperature'].Update(str(temp));
    if temp > 5 and temp < 40:
      window['TemperatureStatus'].Update("OK", background_color="green")
    else:
      window['TemperatureStatus'].Update("FAIL - check U7.", background_color="red")
      check_ok = 0

    window.Read(timeout=0)  #non-blocking update of GUI elements


    ## Checking solar:

    write_register(tester_pcb_addr, 0x22, 0)
    time.sleep(0.5)

    solar_idle1 = read_register(tester_pcb_addr, 0x06) * 3.3 / 512.0
    solar_idle2 = read_register(tester_pcb_addr, 0x07) * 3.3 / 512.0
    window['Solar-idle'].Update('{:.2f}'.format(solar_idle1) + "," + '{:.2f}'.format(solar_idle2));
    if solar_idle1 > 1.5 and solar_idle2 > 1.5:
      window['Solar-idleStatus'].Update("OK", background_color="green")
    else:
      window['Solar-idleStatus'].Update("FAIL - check U6.", background_color="red")
      check_ok = 0

    write_register(tester_pcb_addr, 0x22, 1)
    time.sleep(0.5)

    solar_charge1 = read_register(tester_pcb_addr, 0x06) * 3.3 / 512.0
    solar_charge2 = read_register(tester_pcb_addr, 0x07) * 3.3 / 512.0
    window['Solar-chrg'].Update('{:.2f}'.format(solar_charge1) + "," + '{:.2f}'.format(solar_charge2));
    if solar_charge1 > 1.5 and solar_charge2 > 1.5:
      window['Solar-chrgStatus'].Update("OK", background_color="green")
    else:
      window['Solar-chrgStatus'].Update("FAIL - check U6.", background_color="red")
      check_ok = 0

    window.Read(timeout=0)  #non-blocking update of GUI elements


    ## VGSM:
    write_register(silo_pcb_addr, 0x26, 1)

    voltage = read_register(tester_pcb_addr, 0x01) * 3.3 / 128.0
    window['VGSM'].Update('{:.2f}'.format(voltage) + " Volt");
    if voltage < 3.0:
      window['VGSMStatus'].Update("FAIL - check M2.", background_color="red")
    window['VGSMStatus'].Update("SUCCESS", background_color="green")


    window.Read(timeout=0)  #non-blocking update of GUI elements

    ## VStepUp:
    write_register(silo_pcb_addr, 0x01, 0)
    write_register(silo_pcb_addr, 0x00, 1)
    time.sleep(0.5)
    voltage_low = read_register(tester_pcb_addr, 0x00) * 3.3 * 11 / 256

    write_register(silo_pcb_addr, 0x01, 1)
    time.sleep(0.5)
    voltage_high = read_register(tester_pcb_addr, 0x00) * 3.3 * 11 / 256
    write_register(silo_pcb_addr, 0x00, 0)
 
    window['VStepUp'].Update('{:.2f}'.format(voltage_low) + " V, " + '{:.2f}'.format(voltage_high) + " V");
    if voltage_low < 6.0 or voltage_low > 12 or voltage_high < 25.0 or voltage_high > 32:
      window['VStepUpStatus'].Update("FAIL - check M3, U5.", background_color="red")
    window['VStepUpStatus'].Update("SUCCESS", background_color="green")

    window.Read(timeout=0)  #non-blocking update of GUI elements


    ## Summary:
    if check_ok == 1:
      window['Result'].Update("Testing successful!", background_color="green"); 
    else:
      window['Result'].Update("Testing failed!", background_color="red"); 



#
# Main execution stream
#

os.chdir(os.path.dirname(os.path.abspath(__file__)))

layout = [
  [sg.Text('VSystem:',        size=(20, 1)), sg.Text('-', size=(20,1), key='VSystem'),       sg.Text('', size=(50,1), key='VSystemStatus')],
  [sg.Text('Accelerometer: ', size=(20, 1)), sg.Text('-', size=(20,1), key='Accelerometer'), sg.Text('', size=(50,1), key='AccelerometerStatus')],
  [sg.Text('Temperature: ',   size=(20, 1)), sg.Text('-', size=(20,1), key='Temperature'),   sg.Text('', size=(50,1), key='TemperatureStatus')],
  [sg.Text('Solar-idle:',     size=(20, 1)), sg.Text('-', size=(20,1), key='Solar-idle'),    sg.Text('', size=(50,1), key='Solar-idleStatus')],
  [sg.Text('Solar-chrg:',     size=(20, 1)), sg.Text('-', size=(20,1), key='Solar-chrg'),    sg.Text('', size=(50,1), key='Solar-chrgStatus')],
  [sg.Text('VGSM:',           size=(20, 1)), sg.Text('-', size=(20,1), key='VGSM'),          sg.Text('', size=(50,1), key='VGSMStatus')],
  [sg.Text('VStepUp:',        size=(20, 1)), sg.Text('-', size=(20,1), key='VStepUp'),       sg.Text('', size=(50,1), key='VStepUpStatus')],
  [sg.Button('Program'), sg.Button('Run Test'), sg.Button('Flash Firmware'), sg.Text(' ', size=(80,1), key='Result', justification="center"), sg.Exit()]]

window = sg.Window('SmarterSilo: Silo-Main Tester v1.0 - BrickXter GmbH', layout, finalize=True)      

#last_id = "1912345"

while True:
    event, values = window.Read()

    if event == "Run Test":
      run_test(window)

    if event == "Program":
      program_pcb(window)

    if event == "Flash Firmware":
      text = sg.PopupGetText("Please enter SmarterSilo sensor ID:", "Sensor ID required")
      if len(text) < 7:
        window['Result'].Update("Abort: Invalid sensor ID " + sensoridstring, background_color="red")
      else:
        write_firmware(window, int(text))

    if event in (None, 'Exit'):
      break

window.Close()
