
import smbus
import time
import numpy

# Get I2C bus
bus = smbus.SMBus(1)

#
# Part 1: Read values from tester PCB:
#

addr_tester = 0x41

# Read voltages:

for i in range(0,7):
  voltage  = bus.read_byte_data(addr_tester, 0x00) / 128.0 * 3.3;
  print("Analog channel " + str(i) + ": " + str(voltage) + " Volt")


dummy = bus.read_byte_data(addr_tester, 0x09)
print(dummy)


bus.write_byte_data(addr_tester, 0x22, 1)

dummy = bus.read_byte_data(addr_tester, 0x22)
print(dummy)


