
import PySimpleGUI as sg
import os
import serial
from enum import Enum

import time

serial_log = ''


class TestStage(Enum):
  AT=0
  QGNSSC0=1
  QGNSSC=2
  CPIN=3
  CREG=4
  CSQ=5
  CGATT=6
  CGATT2=7
  QGNSSC2=8
  QGNSSRD=9
  DONE=10

def run_test(window, serial_log):
    #
    # Reset text fields
    #
    neutral_color = "grey"
    window['AT-Status'].Update("", background_color=neutral_color); 
    window['CPIN-Status'].Update("", background_color=neutral_color); 
    window['CREG-Status'].Update("", background_color=neutral_color); 
    window['CSQ-Status'].Update("", background_color=neutral_color); 
    window['CGATT-Status'].Update("", background_color=neutral_color); 
    window['GPS-Time-Status'].Update("", background_color=neutral_color); 
    
    window['Result'].Update("Running tests...", background_color="yellow"); 
    window['runtest'].Update("Abort"); 
    window.Read(timeout=0)  #non-blocking update of GUI elements

    #
    # Read current values and set text fields accordingly
    #

    check_ok = 1

    ser = serial.Serial('/dev/ttyAMA0', 19200, timeout = 1) # ttyACM1 for Arduino board
    test_in_progress = 1
    test_stage = TestStage.AT

    while test_in_progress == 1 and check_ok == 1:

      # only send if there is nothing waiting in input buffer
      if ser.in_waiting == 0:
        
        if test_stage == TestStage.AT:
          command = "AT\n"
        if test_stage == TestStage.QGNSSC0:
          command = "AT+QGNSSC=0\n"
        if test_stage == TestStage.QGNSSC:
          command = "AT+QGNSSC=1\n"
        if test_stage == TestStage.CPIN:
          command = "AT+CPIN?\n"
        if test_stage == TestStage.CREG:
          command = "AT+CREG?\n"
        if test_stage == TestStage.CSQ:
          command = "AT+CSQ\n"
        if test_stage == TestStage.CGATT:
          command = "AT+CGATT=1\n"
        if test_stage == TestStage.CGATT2:
          command = "AT+CGATT=1\n"
        if test_stage == TestStage.QGNSSC2:
          command = "AT+QGNSSC=1\n"
        if test_stage == TestStage.QGNSSRD:
          command = "AT+QGNSSRD?\n"
        
        #print ("Writing: ",  command)
        ser.write(str(command).encode())
        serial_log = "[Out] " + command + serial_log;
        window['SerialLog'].Update(serial_log)
        window.Read(timeout=0)  #non-blocking update of GUI elements
        time.sleep(0.5)

      # process incoming
      try:
        readOut = ser.readline().decode('ascii')
        #print ("Reading: ", readOut) 
        serial_log = "[In] " + readOut + serial_log;
        window['SerialLog'].Update(serial_log)
       
        event, values = window.Read(timeout=0)  #non-blocking update of GUI elements
        
        # Abort if user requests so:
        if event == "runtest":
          check_ok = 0
          window['runtest'].Update("Run Test")
          break
        
        time.sleep(0.1)

        # advance to next test stage if necessary:
        if test_stage == TestStage.AT and readOut.startswith("OK"):
          window['AT-Status'].Update("SUCCESS!", background_color="green"); 
          test_stage = TestStage.QGNSSC0
        elif test_stage == TestStage.QGNSSC0 and (readOut.startswith("OK") or readOut.startswith("+CME")):
          test_stage = TestStage.QGNSSC
        elif test_stage == TestStage.QGNSSC and (readOut.startswith("OK") or readOut.startswith("+CME")):
          test_stage = TestStage.CPIN
        elif test_stage == TestStage.CPIN and "READY" in readOut:
          window['CPIN-Status'].Update("SUCCESS!", background_color="green"); 
          test_stage = TestStage.CREG
        elif test_stage == TestStage.CPIN and "ERROR" in readOut:
          window['CPIN-Status'].Update("ERROR! - Check U2 and SIM slot", background_color="red"); 
          check_ok = 0
          break
        elif test_stage == TestStage.CREG and "0,5" in readOut:
          window['CREG-Status'].Update("SUCCESS!", background_color="green"); 
          test_stage = TestStage.CSQ
        elif test_stage == TestStage.CSQ:
          csq = readOut.split("CSQ: ")[1];          # get '23,0' part of '+CSQ: 23,0'
          signal_strength = int(csq.split(",")[0])  # get 23 from '23,0'
          if signal_strength >= 10:
            window['CSQ-Status'].Update("SUCCESS!", background_color="green"); 
            test_stage = TestStage.CGATT
        elif test_stage == TestStage.CGATT and readOut.startswith("OK"):
          test_stage = TestStage.CGATT2
          readOut = "empty"
        elif test_stage == TestStage.CGATT2 and readOut.startswith("OK"):
          window['CGATT-Status'].Update("SUCCESS!", background_color="green"); 
          test_stage = TestStage.QGNSSC2
        elif test_stage == TestStage.QGNSSC2 and readOut.startswith("AT+QGNSSC"):
          test_stage = TestStage.QGNSSRD
        elif test_stage == TestStage.QGNSSRD and readOut.startswith("+QGNSSRD:"):
          gpstime = readOut.split(",")[1];
          gpstimeint = int(gpstime.split(".")[0])
          if gpstimeint < 230000 and gpstimeint > 10000:
            test_stage = TestStage.DONE
            window['GPS-Time-Status'].Update(gpstime[0:2] + ":" + gpstime[2:4] + ":" + gpstime[4:6] + "  SUCCESS!", background_color="green"); 
          else:
            window['GPS-Time-Status'].Update(gpstime[0:2] + ":" + gpstime[2:4] + ":" + gpstime[4:6], background_color=neutral_color); 
        elif test_stage == TestStage.QGNSSRD and (readOut.startswith("$GPGSV") or readOut.startswith("$GLGSV")):
          num_satellites = int(readOut.split(",")[3])
          print(num_satellites)
          if num_satellites > 0:
            test_stage = TestStage.DONE
            window['GPS-Time-Status'].Update("Satellite found: SUCCESS!", background_color="green");
        elif test_stage == TestStage.DONE:  # finished
          break
          
      except:
        pass

      ser.flush() #flush the buffer

    if check_ok == 1:
      window['Result'].Update("Testing successful!", background_color="green"); 
    else:
      window['Result'].Update("Testing failed!", background_color="red"); 

    window['runtest'].Update("Run Test"); 


#
# Main execution stream
#

os.chdir(os.path.dirname(os.path.abspath(__file__)))

layout = [[sg.Text('AT: ',     size=(20, 1)),  sg.Text('', size=(50,1), key='AT-Status')],
          [sg.Text('AT+CPIN: ', size=(20, 1)), sg.Text('', size=(50,1), key='CPIN-Status')],
          [sg.Text('AT+CREG: ', size=(20, 1)), sg.Text('', size=(50,1), key='CREG-Status')],
          [sg.Text('AT+CSQ:',   size=(20, 1)), sg.Text('', size=(50,1), key='CSQ-Status')],
          [sg.Text('AT+CGATT:', size=(20, 1)), sg.Text('', size=(50,1), key='CGATT-Status')],
          [sg.Text('GPS-Time:', size=(20, 1)), sg.Text('', size=(50,1), key='GPS-Time-Status')],
          [sg.Multiline(key='SerialLog', size=(100, 20))],
          [sg.Button('Run Test', key="runtest"), sg.Text('', size=(80,1), key='Result', justification="center"), sg.Exit()]]

window = sg.Window('SmarterSilo: GSM MC690 Tester v1.1 - BrickXter GmbH', layout, finalize=True)      


while True:
    event, values = window.Read()

    print(event)
    if event == "runtest":
      serial_log = ''
      run_test(window, serial_log)

    if event in (None, 'Exit'):
      break

window.Close()
