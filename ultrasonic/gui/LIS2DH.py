import smbus
import time

# Get I2C bus
bus = smbus.SMBus(1)
addr = 0x18

# Set accelerometer active:
bus.write_byte_data(addr, 0x20, 0x27)
bus.write_byte_data(addr, 0x23, 0x08)

while 1:

  time.sleep(0.5)
  # Read 7 bytes from device, starting at register 0x00:
  #status, xdata1, xdata0, ydata1, ydata0, zdata1, zdata0 = bus.read_i2c_block_data(addr, 0x27, 7)
  status = bus.read_byte_data(addr, 0x27)
  print "Status: %d" %status

  # Convert the data
  xdata0 = bus.read_byte_data(addr, 0x28)
  xdata1 = bus.read_byte_data(addr, 0x29)
  xAccl = xdata1 * 256 + xdata0
  if xAccl > 32767 :
	xAccl -= 65536

  ydata0 = bus.read_byte_data(addr, 0x2A)
  ydata1 = bus.read_byte_data(addr, 0x2B)
  yAccl = ydata1 * 256 + ydata0
  if yAccl > 32767 :
  	yAccl -= 65536

  zdata0 = bus.read_byte_data(addr, 0x2C)
  zdata1 = bus.read_byte_data(addr, 0x2D)
  zAccl = zdata1 * 256 + zdata0
  if zAccl > 32767 :
	zAccl -= 65536

  # Output data
  print "Acceleration in X-Axis : %d" %xAccl
  print "Acceleration in Y-Axis : %d" %yAccl
  print "Acceleration in Z-Axis : %d" %zAccl

